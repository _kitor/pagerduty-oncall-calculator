#!/usr/bin/env python
import click
from dotenv import load_dotenv
from pdpyras import APISession
from datetime import datetime, timedelta
from calendar import monthrange
from zoneinfo import ZoneInfo
from collections import defaultdict

load_dotenv()

previous_month = datetime.now().replace(day=1) - timedelta(days=1)

@click.option('--token', required=True, envvar='TOKEN')
@click.option('--year', default=previous_month.year, show_default=previous_month.year)
@click.option('--month', default=previous_month.month, show_default=previous_month.month)
@click.option('--debug', is_flag=True)
@click.option('--person', default=None)
@click.option('--ignore', default=None, multiple=True)
@click.command()
def on_calls(token, year, month, debug, person, ignore):
    pd = APISession(token)
    pd.url = 'https://api.eu.pagerduty.com'

    users = defaultdict(lambda: set())
    for oncall in pd.list_all('oncalls', params={'since': f'{year}-{month}-01'}):
        user = oncall['user']['summary']

        if person and person != user:
            continue

        if 'schedule' in oncall.keys() and oncall['schedule'] and oncall['schedule']['summary'] in ignore:
            continue

        if oncall['start'] is None or oncall['end'] is None:
            continue

        start = datetime.fromisoformat(oncall['start'])
        end = datetime.fromisoformat(oncall['end'])
        if debug:
            print("start {} end {}; {}".format(start, end, oncall['schedule']['summary']))
        month_end = datetime(year, month, monthrange(year, month)[1], 23, 59, 59, tzinfo=ZoneInfo('Europe/Warsaw')) + timedelta(seconds=1)
        month_start = datetime(year, month, 1, 0, 0, 0, tzinfo=ZoneInfo('Europe/Warsaw'))
        if start > month_end:
            continue
        users[user].add((min(end, month_end), max(start, month_start)))

    for name, timespans in users.items():
        hours = 0
        for end, start in timespans:
            count = (end-start).total_seconds() / 3600
            if debug:
                print("start {} end {}; {}".format(start, end, count))
            hours += count
        print(name, (hours))

if __name__ == '__main__':
    on_calls()
