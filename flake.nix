{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.poetry2nix = {
    url = "github:nix-community/poetry2nix";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, poetry2nix, flake-utils }: flake-utils.lib.eachDefaultSystem(system: let
      pkgs = nixpkgs.legacyPackages.${system};
      p2nix = poetry2nix.lib.mkPoetry2Nix { inherit  pkgs; };
      poetry_args = {
          projectDir = ./.;
          overrides = p2nix.overrides.withDefaults (self: super: {
            pdpyras = super.pdpyras.overridePythonAttrs (old: {
              nativeBuildInputs = old.nativeBuildInputs ++ [self.setuptools];
            });
          });
      };
  in {
      packages.pd-oncall = p2nix.mkPoetryApplication poetry_args;
      packages.default = self.packages.${system}.pd-oncall;
      apps.default = { type = "app"; program = "${self.packages.${system}.default}/bin/pd-oncall"; };
      devShells.default = pkgs.mkShellNoCC {
          packages = [ pkgs.poetry (p2nix.mkPoetryEnv poetry_args) ];
      };
  });
}
